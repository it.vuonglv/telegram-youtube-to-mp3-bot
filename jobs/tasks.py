import os
import random
import time
from typing import Dict, Union, Any

import requests
from celery import Task

from jobs.worker import app
from jobs.logic import send_audio
from jobs.config import Config


@app.task
def download_and_convert():
    t = random.randrange(1, 30)
    time.sleep(t) # Mo phong qua trinh download va convert
    return {
        "done_in": t
    }

@app.task(bind=True)
def send_audio_async(self: Task,
                     chat_id: str,
                     video_id: str) -> Dict[str, Any]:
    try:
        self.update_state(state="PROCESSING",
                        meta={
                            "current": 50,
                            "status": "Processing file"
                        })
        result = send_audio(Config, chat_id, video_id)
    except Exception as ex:
        return {
            "success": False,
            "error": repr(ex)
        }
    else:
        return {
            "current": 100,
            "status": "Done, sent audio",
            "result": result
        }


@app.task(bind=True)
def send_message_after(self: Task,
                       chat_id: str,
                       message: str,
                       delay: int = 5) -> Dict[str, Union[str, int]]:
    token = os.getenv("TELEGRAM_BOT_TOKEN")
    url = f"https://api.telegram.org/bot{token}/sendMessage"
    payload = {
        "chat_id": chat_id,
        "text": message
    }

    self.update_state(state="DOWNLOADING",
                      meta={
                          "current": 50,
                          "status": "downloading something"
                      })

    time.sleep(delay)

    self.update_state(state="SENDING",
                      meta={
                          "current": 75,
                          "status": "message is sending"
                      })
    time.sleep(1)
    resp = requests.get(url=url, params=payload)
    return {
        "current": 100,
        "status": "message sent",
        "result": resp.json()
    }
