import os

accept_content = ['json']
include = ['jobs.tasks']
broker_url = os.getenv("REDIS_URL", "localhost:6379")
result_backend = os.getenv("BACKEND_URL")
broker_pool_limit = 20
#worker_pool = "gevent"