import datetime
from datetime import timedelta
from typing import Any, Dict

from celery.result import AsyncResult
from flask import Flask, abort, jsonify, request, url_for

from jobs.tasks import download_and_convert, send_message_after
from jobs.tasks import send_audio_async

Json = Dict[str, Any]

app = Flask("api")

@app.route("/")
def hello():
    download_and_convert.delay()
    return jsonify(status="ok", message="hello babe")

@app.route("/audio/send")
def send_audio():
    chat_id = request.args.get("chat_id")
    video_id = request.args.get("video_id")
    if not chat_id or not video_id:
        response = {
            "status": "failed",
            "code": 404,
            "message": "require chat_id and video_id"
        }
        return jsonify(response), 404
    task = send_audio_async.apply_async(args=(chat_id, video_id))
    response = {
        "status": "success",
        "params": {
            "chat_id": chat_id,
            "video_id": video_id
        },
        "code": 200,
        "message": "application is processing your request"
    }
    headers = {
        "Location": url_for("taskstatus", task_id=task.id)
    }
    return jsonify(response), 200, headers

@app.route('/send_message/<chat_id>/<delay>')
def send_message(chat_id: str, delay: int) -> Json:
    try:
        delay = int(delay)
        now = datetime.datetime.utcnow()
        message = request.args.get("message", now.isoformat())
        task = send_message_after.apply_async(args=(chat_id, message, delay))
        result = {
            "status": "success",
            "chat_id": chat_id,
            "message": message,
            "start": now.isoformat(),
            "delay": delay
        }
        headers = {
            "Location": url_for("taskstatus", task_id=task.id)
        }
        return jsonify(result), 200, headers
    except Exception as ex:
        abort(500)

@app.route("/status/<task_id>")
def taskstatus(task_id: str) -> Json:
    task: AsyncResult = send_message_after.AsyncResult(task_id)
    state = task.state
    if state == "PENDING":
        response = {
            'state': state,
            'current': 0,
            'status': "Pending"
        }
    elif state != "FAILURE":
        info = task.info
        response = {
            'state': state,
            'current': info.get("current", 0),
            'status': info.get("status", "")
        }
        if "result" in info:
            response["result"] = info.get("result")
    else:
        # ERROR
        response = {
            "state": state,
            "current": 1,
            "status": str(task.info)
        }
    return jsonify(response)
