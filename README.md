# telegram-youtube-to-mp3-bot


## Setting  
### environment variables  
`REDIS_URL`: uri redis for celery  
`TELEGRAM_BOT_TOKEN`: token for telegram bot  
`BOT_TOKEN`: token for audio bot  
`MUSIC_CHANNEL_ID`: music channel  
`BACKEND_URL`: celery backend url

## start api  
`gunicorn api.app:app -w 5 -k gevent`  

## start celery workers  
`celery -A jobs.worker.app worker -l info`

